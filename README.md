# Agnostic UI Approach

The agnostic UI is a philosophical approach to frontend web development was created because of the following pain point:

- Front-end development is growing in complexity and your typical application developer and content editor does not possess the knowledge needed to properly build out or manage front-end assets, scripts or HTML.
- Server side technologies are not all equal in how they generate their front-end markup. They are also to inconsistent across the board with many generating on or more of the following problem: 
  - Unneeded DOM markup
  - Unreadable or unusable class names and attribute that can change depending on internal mechanisms.
  - Random JavaScript that is directly controllable by developers
  - Lack appropriate accessibility hooks

- Browser are constantly changing, but our application and static sites are not as portable. In some case, properties do not get refreshes until their base platforms become security issues or are completed unsupported. At which point to bring even the front-end up to standard may require just as much or even more effort to update to meet the requirements of standard web design.

Because of these issues, Tax has developed the concept of having our backend platform generates and serves page JSON rather than standard hardcoded HTML or any HTML that might be created by any platform specific custom tag. This has provided the following benefits

- A very simple outer template that contains only the most basic starting elements to properly layout a page and is much easier to maintain.
- All page structural data, meta data and actual contents are stored inside of a commonly accessible global object.
- The presentation and interaction layers are completely abstracted from base platform giving front-end developers 100% freedom to use any library, framework, or custom code they wish to render the page.
- The platform is also made more modular at this point as they can easily swap out modules at anytime as long as those components still allow the proper JSON output. 

## Page JSON

Page JSON is all the initial structure, page context, and content needed to render a given page. Additional resources should be lazy loaded or initialized as needed. The initial structure should looks something similar to this:		

### Example Page JSON

```javascript
<script>
var pageJSON = {
    "header": {
     	"contents": [
            // All your header JSON
     	]   
    },
    "page": {
        "contents": [
        	// All your page contents
        ]
    },
    "footer": {
        "contents": [
            // All your footer contents
        ]
    },
    "context": {
        "platform": {
            // Platform specific infomation you wish to expose
        },
        "ui: {
            // User Interface specific information
        }
    }
}
</script>
```

These are what we consider to be the core properties, depending on the individual needs, additional properties can be added to fit platform specific requirements. Everything found in the header, page and footer property contents array should then follow the specification for node contents. 

## Node Specifications

Nodes are the actual DOM components, or elements that will be rendered by the UI framework or render of choice. While the node specification is very simple for speed, you should put as much of initial content and setting you need to render a particular node up front. The reason for this is if you are writing a custom render should be for most intensive purposes dumb. Leave any logic you have to your rendering scripts or templating engine. Here are the requirements for each node:

- Each node is an object, and should only appear inside contents array, with the only exception existing when templates have specific expectations for sub content that are directly related to is parent. Think about field set and input groups!
- Each node should declare what type of node it is, they should do this by using the "template" property.
- Each node also has a reserved "type" property that be used as high level pivot so common shared template time can make internal decisions as needed.
- Whenever possible, abstract values down to special keywords and properties and not to hard code items like class names and actual tag types.

Lets look at an example used to generate a simple responsive row:

```javascript
{
    "type": "row",
    "template": "grid",
    "contents": [
        {
            "type": "column",
            "template": "grid,
            "contents": []
        },
        {
            "type": "column",
            "template": "grid,
            "contents": []
        },
    ]
}
```

In our above example, we used the same rendering template to generate both grids rows and columns, and the common type property is being used for pivot between the two desired node. If we were to processes node we would expect to see something like:

```html
<div class="responsive-row">
	<div class="column-full"></div>
	<div class="column-full"></div>
</div>
```

The biggest thing to note here is the class and the div tags. The templating engine knows when rendering a row or a column to use a div tag. It also know that whenever a row is used, to add the class of "responsive-row" and a column is called, to use "column-full".

Now depending on what you want to render, some node JSON is going to need to be more specific. For example, lets look at a simple HTML input. Each input should have its own label as well as an actual input. There are also some items that need to exist like specific HTML attributes like the "for" attribute of a label has to match the "id" of the input element. Here is a quick example of some JSON:

```JSON
{
    "type": "text",
    "template": "field",
    "label": {
        "attribute": {
            "for": "text-input"
        },
        "text": "Text Input:"
    },
    "input": {
        "attribute": {
            "type": "text",
            "id": "text-input",
            "name": "textfield",
            "value": "Value!"
        }
    }
}
```

Lets break this down.

The above node is using a single common template of "field" ( which we can assume is used for all field types because it has the common type property set to "text"). Also note how there is no contents array in this node. This is because inputs tend to be the last item in a DOM structure, but we do have new high level node properties of label and input. As stated above if a node template is going to preform all of the needed processing, the contents property is not required. Since out template engine for the purposes of this example, will know how to render the request label and input directly, we just added those properties to the current node directly. 

Now lets look at label and input. Both object have the attributes property. To keep our render simple, we teach it how attributes work and the templating engine know right were to place those attribute at render time. This follows our main rule of requiring all all context to be present up front in the page JSON. 

But we also have some redundancy, the "type": "text" property and the "for" and "id" properties have been repeated, may appear to be a mistake; it's not. As stated before, renders should be dumb, and while the repeated fields do add additional payload wait to the JSON we also don't have to teach the templating engine or add special code to the render when rendering those field. (So its a trade off!)

In its simplest state the above JSON would render like so:

```html
<label for="text-input">Text Input:</label>
<input type="text" id="text-input" name="textfield" value="Value!" />
```

But that is a simple example, let assume our template engine also knows how to properly wrap our fields in a defined form layout. The same JSON could still be used, but we could get this output just as easily!

```html
<div class="field">
	<div class="label>
		<label for="text-input">Text Input:</label>
	</div>
	<div class="input">
		<input type="text" id="text-input" name="textfield" value="Value!" />
	</div>
</di>
```

## Currently Defined Nodes

More defined Nodes to come!